#!/bin/bash
# Profile file. Runs on login.

# Adds `~/.local/bin` and all subdirectories to $PATH
#export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
export PATH="$(find "$HOME/.local/bin/" -type d -printf '%p:')$PATH"
export EDITOR="nvim"
export BROWSER="firefox"
#export READER="zathura"

# Some environments do not set this correctly, so make sure it is here
# Looking at you, Qt/KDE
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"


# Make Programs repect XDG where possible
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export SCREENRC="$XDG_CONFIG_HOME/screen/screenrc"
export LESSHISTFILE=-
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority

# less/man colors
# Will take a look at these
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"

echo "$0" | grep "bash$" >/dev/null && [ -f ~/.bashrc ] && source "$HOME/.bashrc"

# Ruby gems not polluting the system
export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
export PATH="$PATH:$GEM_HOME/bin"

# Switch escape and caps if tty:
# This sounds awesome, might want to try that
# Take a look at https://github.com/LukeSmithxyz/voidrice to find config
#sudo -n loadkeys ~/.scripts/ttymaps.kmap 2>/dev/null

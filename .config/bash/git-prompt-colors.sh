# This theme for gitprompt.sh is optimized for the "Solarized Dark" and "Solarized Light" color schemes
# based on "Solarized Extravagant", with user@host on the second line and some things removed.

function override_git_prompt_colors() {
  GIT_PROMPT_THEME_NAME='Custom'
  GIT_PROMPT_COMMAND_OK="${Green}✔"
  GIT_PROMPT_COMMAND_FAIL="${Red}_LAST_COMMAND_STATE_✘"
  if [ $UID = 0 ]
  then
    GIT_USER_COLOR=${Red}
  else
    GIT_USER_COLOR=${Lightgreen}
  fi
  GIT_PROMPT_START_USER="${GIT_USER_COLOR}\\u${Reset}@${Lightblue}\\h${Reset} [_LAST_COMMAND_INDICATOR_${Reset}|"
  GIT_PROMPT_START_ROOT="${GIT_PROMPT_START_USER}"
  GIT_PROMPT_END_USER=" ${Yellow}${PathShort}${Reset}\n\$${Reset} "
  GIT_PROMPT_END_ROOT="${GIT_PROMPT_END_USER}"
  GIT_PROMPT_LEADING_SPACE=1
  GIT_PROMPT_PREFIX=""
  GIT_PROMPT_SUFFIX="]"
  GIT_PROMPT_SYMBOLS_NO_REMOTE_TRACKING="✭"
}

reload_git_prompt_colors 'Custom'

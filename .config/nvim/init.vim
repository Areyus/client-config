" A leader let's us map more buttons, so we should have one
" Let's have a leader thats sane on all keyboards layouts
let mapleader =","

" Ensure we have vim-plug installed to load the plugins
if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

" Load plugins here
" Plugins are installed via vim-plug
" :PlugInstall to install new plugins
" :PugUpdate to update existing ones
call plug#begin('~/.config/nvim/plugged')
Plug 'https://github.com/rodjek/vim-puppet' " Puppet-friendly vim
Plug 'https://github.com/itchyny/lightline.vim' " Statusline in nice
Plug 'https://github.com/tpope/vim-fugitive' " Easy git in vim
Plug 'https://github.com/tpope/vim-surround' " Surround things with ease
Plug 'https://github.com/preservim/nerdtree' " Powerfull filesystem explorer in vim
Plug 'https://github.com/Xuyuanp/nerdtree-git-plugin' " Make nerdtree git-aware
Plug 'https://github.com/vimwiki/vimwiki' " Wiki within vim
Plug 'https://github.com/tpope/vim-commentary' " Make commenting in and out easier
Plug 'https://github.com/godlygeek/tabular' " Easy text alighnment, used by vim-puppet
Plug 'https://github.com/dense-analysis/ale' " Linting, Codecompletion, and more
call plug#end()

" Configure Plugins
let g:ale_fixers = {
\    'puppet': [
\        'puppetlint',
\    ]
\}

" Make tabs more friendly
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab " Ensure indent spaces at start of line are deleted in shiftwidth groups too
set autoindent
set smartindent
set noshowmode " Mode showing is unneeded due to mode beeing in statusline

"" Ensure colors are usefull on my terminals and make them look good
set background=dark
set termguicolors

"" Allow plugins to define filetypes
filetype plugin on

"" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
set splitbelow splitright

"" set utf8 as default encoding (why would you not want this?)
set encoding=utf8

"" Turn backups, swaps, etc off
set nobackup
set nowb
set noswapfile

"" in wildmenu, give list of on first tab, on second rotate through options
"" as a reminder, wildmenue is the autocomplete for commands
set wildmode=list:longest,full

"" Mouse support for vim is stupid and breaks my workflows
"" Just make sure it's off
set mouse=

"" Copy to clipboard via yank
set clipboard=unnamedplus

"" Get the very usefull (at least with vim) relative linenumbers
set number relativenumber

" Required for some nice stuff, especially plugin related
" Technically should not be needed, but better save than sorry
" nvim is never compatible, vim is sometimes...
set nocompatible

" Make paste option toggleable via CTRL+p
set pastetoggle=<C-p>

" Make search somewhat interactive
set incsearch

" Open dialogue for saving instead of just failing on unsaved files
set confirm

" Interprete markdown as markdown
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

" :W sudo saves the file
" (useful for handling the permission-denied error)
command W w !sudo tee % > /dev/null

" Disable highlights with leader-enter
map <silent> <leader><cr> :noh<cr>

" make window movement easier
map <leader><Down> <C-W>J
map <leader><Up> <C-W>K
map <leader><Left> <C-W>H
map <leader><Right> <C-W>L

" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove
map <leader>t<leader> :tabnext<cr>

" Let 'tl' toggle between this and the last accessed tab
let g:lasttab = 1
nmap <Leader>tl :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()


" Opens a new tab with the current buffer's path
" Super useful when editing files in the same directory
map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" Switch CWD to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>:pwd<cr>

" Return to last edit position when opening files
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Re-set cursor to first line if editing git commits
" This is dirty, but so are all other solutions I found
au Filetype gitcommit :norm 1G1|

" Move a line of text using ALT+[jk] or Command+[jk] on mac
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z<Paste>

" Delete trailing white space on save, useful for some filetypes ;
" Maybe expand this to more types someday. Never include md though...
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun

if has("autocmd")
    autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
endif

""""""""""""""""""""""""""""""""""""""""""""""
" Maybe evaluate this someday
" Not sure if I want to use these
""""""""""""""""""""""""""""""""""""""""""""""

" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

" Turn persistent undo on 
" means that you can undo even when you close a buffer/VIM
try
    set undodir=~/.config/nvim/undodir
    set undofile
catch
endtry

" CTRL+i will switch whether searches and replaces are case sensitive
" I would really like this, but it breaks manually interting spaces via tabs
" (I dont get it)
"nnoremap <C-i> :set ignorecase! ignorecase?<CR>
"imap <C-i> <C-O><C-i>


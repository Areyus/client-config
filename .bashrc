#!/bin/bash
# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Basic options
stty -ixon # Disable ctrl-s and ctrl-q.
shopt -s autocd #Allows you to cd into directory merely by typing the directory name.

# make Color codes human readable
# Might want to switch some of these, will see
Black='\[\e[0;30m\]'
Darkgrey='\[\e[1;30m\]'
Lightgrey='\[\e[0;37m\]'
White='\[\e[1;37m\]'
Red='\[\e[0;31m\]'
Lightred='\[\e[1;31m\]'
Green='\[\e[0;32m\]'
Lightgreen='\[\e[1;32m\]'
Brown='\[\e[0;33m\]'
Yellow='\[\e[1;33m\]'
Blue='\[\e[0;34m\]'
Lightblue='\[\e[1;34m\]'
Purple='\[\e[0;35m\]'
Lightpurple='\[\e[1;35m\]'
Cyan='\[\e[0;36m\]'
Lightcyan='\[\e[1;36m\]'
Reset='\[\e[0m\]'

# History control

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=
HISTFILESIZE=
HISTTIMEFORMAT='%F %T '

#Shorten multiline commands to one line in history
shopt -s cmdhist

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
# Not sure if I want this, yet
shopt -s globstar

# Ensure that shell sessions are english
# I don't like man/shell commands in my native language
export LANG=en_US.utf8
export LANGUAGE=en_US

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Set the terminal prompt. This will result in a colored version of:
# user@hostname [$?] : $dirname
# $
# some people don't like multiline prompt, but it get's awesome once you use things like gitprompt with long dirnames
if test "$UID" = 0
then
  PS1="${Lightred}\\u${Reset}@${Lightred}\\h${Reset} [${Lightgreen}\$?${Reset}] : \\w\n\\$ ${Reset}"
else
  PS1="${Lightgreen}\\u${Reset}@${Lightblue}\\h${Reset} [${Lightgreen}\$?${Reset}] : \\w\n\\$ ${Reset}"
fi

# Outsourcing of alias and function definitions
# These could take up way to much space here
if [ -f ~/.config/bash/aliases ]; then
    . ~/.config/bash/aliases
fi
if [ -f ~/.config/bash/functions ]; then
    . ~/.config/bash/functions
fi

# Source a proxy config if available
if [ -f ~/.config/bash/proxy ]; then
    . ~/.config/bash/proxy
fi

# Source a file for local only configs you might want to add yourself
if [ -f ~/.config/bash/local ]; then
    . ~/.config/bash/local
fi

# Source a file for WSL specific things you do not want/need on native
if [[ $(grep Microsoft /proc/version) ]] && [ -f ~/.config/bash/wsl ]; then
    . ~/.config/bash/wsl
fi

# Add git to bash prompt
# All the custimization options go here
GIT_PROMPT_ONLY_IN_REPO=1
# GIT_PROMPT_FETCH_REMOTE_STATUS=0   # uncomment to avoid fetching remote status
# GIT_PROMPT_IGNORE_SUBMODULES=1 # uncomment to avoid searching for changed files in submodules
# GIT_PROMPT_SHOW_UPSTREAM=1 # uncomment to show upstream tracking branch
# GIT_PROMPT_SHOW_UNTRACKED_FILES=all # can be no, normal or all; determines counting of untracked files

# GIT_PROMPT_SHOW_CHANGED_FILES_COUNT=0 # uncomment to avoid printing the number of changed files

# GIT_PROMPT_STATUS_COMMAND=gitstatus_pre-1.7.10.sh # uncomment to support Git older than 1.7.10

# GIT_PROMPT_START="${Lightgreen}\\u${Reset}@${Lightblue}\\h${Reset} [${Lightgreen}\$?${Reset}]"    # uncomment for custom prompt start sequence
# GIT_PROMPT_END=...      # uncomment for custom prompt end sequence

# as last entry source the gitprompt script
# GIT_PROMPT_THEME=Custom # use custom theme specified in file GIT_PROMPT_THEME_FILE (default ~/.git-prompt-colors.sh)
GIT_PROMPT_THEME_FILE=~/.config/bash/git-prompt-colors.sh
# GIT_PROMPT_THEME=Solarized # use theme optimized for solarized color scheme
if [ -f ~/.config/bash/bash-git-prompt/gitprompt.sh ]
then
 . ~/.config/bash/bash-git-prompt/gitprompt.sh
fi

# enable programmable completion features if available
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

if [ -z "$PROMPT_COMMAND" ]
then
  PROMPT_COMMAND="setwindowtitle;histappend"
else
  PROMPT_COMMAND="${PROMPT_COMMAND};setwindowtitle;histappend"
fi
